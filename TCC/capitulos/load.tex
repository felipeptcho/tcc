\mychapter{Otimização do Processamento de Requisições}{chp:balanceamento}

\section{Balanceamento de Carga}
Balanceamento de carga é a habilidade de fazer vários servidores participarem no mesmo serviço e fazerem o mesmo trabalho.
De acordo com \citep{Schlossnagle2004}, existem dois principais motivos que levam um \textit{site} além um único servidor Web:

\begin{itemize}
\item
\textbf{Redundância:} Se um \textit{site} tem um propósito crítico e não pode permitir nem mesmo uma interrupção breve de serviço, ele precisa usar múltiplos servidores Web para redundância.
Por mais caro que seja o \textit{hardware} de um servidor, ele irá eventualmente falhar, precisar ser trocado, ou precisar de manutenção física;

\item
\textbf{Capacidade:} Por outro lado, os \textit{sites} são frequentemente movidos para uma configuração de múltiplos servidores para atender suas crescentes demandas de tráfego.
\end{itemize}

No caso do Kuase~de~Graça, se o serviço for interrompido, mesmo que rapidamente, durante a execução de um leilão, o sistema ficará incapaz de receber lances dos participantes e o produto provavelmente será arrematado incorretamente.
Além disso, uma arquitetura escalável permitirá a adição de novos servidores conforme o número de usuários participantes dos leilões cresça.

Uma configuração de exemplo, com três servidores Web com carga balanceada, pode ser vista na Figura~\ref{fig:balanceamento_carga}.
Quando uma requisição chega da Internet, ela é direcionada para um dos três servidores, de acordo com um determinado algoritmo.
O algoritmo \textit{Round Robin}, por exemplo, usa um servidor por vez, como uma fila circular.

É importante observar que alguns desses algoritmos são não-determinísticos, o que significa que duas requisições consecutivas de um mesmo usuário têm uma grande chance de atingirem dois servidores diferentes.
Se as informações de sessão dos usuários são armazenadas nos servidores da aplicação, elas serão perdidas entre essas duas requisições.
Contudo, no caso do Kuase~de~Graça, o CakePHP fornece um mecanismo embutido que permite armazenar as informações de sessão numa tabela do banco de dados, evitando esse problema.

\begin{figure}[hbt]
\centering
\includegraphics[width=\textwidth]{imagens/balanceamento_carga.png}
\caption{Balanceamento de carga em três servidores Web.}
\label{fig:balanceamento_carga}
\end{figure}

\section{\textit{View Caching}}
\label{sec:view_caching}
Algumas páginas do \textit{site} exibem o mesmo conteúdo toda vez que são acessadas.
Por exemplo, a página ``Fale Conosco'' sempre exibirá um formulário de envio de mensagem.
Em outras páginas, o conteúdo muda após o cadastro ou edição de algum registro.
Por exemplo, a página de depoimentos vai sempre exibir os mesmos depoimentos até que um novo seja cadastrado.
Apesar disso, sempre que uma requisição é recebida, todo o ciclo de requisição do CakePHP (Seção~\ref{sec:ciclo_cakephp}) é executado, consumindo recursos do servidor.

\textit{View Caching} é um mecanismo embutido no CakePHP que, quando ativado e configurado, consegue responder a uma requisição reutilizando a resposta de uma requisição anterior compatível, evitando a execução de todo ciclo da Figura~\ref{fig:mvc}.
A compatibilidade das requisições é verificada de acordo com a URL requisitada, conforme a Figura~\ref{fig:view_caching}.
Se a resposta de uma requisição compatível não é encontrada em \textit{cache}, o ciclo de requisição do CakePHP é executado por completo, e então, a resposta gerada é armazenada, conforme a Figura~\ref{fig:view_caching_no}.

O CakePHP também cuida de limpar o \textit{cache} quando acontece alguma mudança nos dados e as páginas precisam ser geradas novamente.
Por exemplo, quando um novo depoimento é cadastrado no sistema, todas as respostas em \textit{cache} relacionadas à entidade ``Depoimento'' são removidas, inclusive \texttt{/depoimentos}.
Assim, quando o próximo cliente fizer uma requisição \texttt{/depoimentos}, a resposta será gerada com o novo depoimento cadastrado e armazenada em \textit{cache} até a próxima alteração.

\begin{figure}[hbt]
\centering
\includegraphics[width=0.9\textwidth]{imagens/view_caching.png}
\caption{Mecanismo de \textit{View Caching}.}
\label{fig:view_caching}
\end{figure}

\begin{figure}[hbt]
\centering
\includegraphics[width=0.9\textwidth]{imagens/view_caching_no.png}
\caption{Armazenamento da resposta de uma requisição no \textit{View Cache}.}
\label{fig:view_caching_no}
\end{figure}

%\subsection{\textit{View Caching versus HTTP Caching}}
%
%O mecanismo de \textit{View Caching} armazena as páginas no próprio servidor.
%Qualquer requisição
%Já o \textit{cache} do navegador do usuário só consegue.

\subsection{Teste de Carga}
Podem ser vistos na Figura~\ref{fig:benchmark}, os resultados de um teste de carga realizado para verificar a eficiência desse mecanismo.
Foram feitas cem requisições concorrentes da página principal do Kuase~de~Graça com o \textit{View Caching} ativado.
E mais cem requisições concorrentes com o \textit{View Caching} desativado.
O tempo levado para responder todas as requisições no segundo caso foi mais que o dobro que no primeiro caso, o que faz esse mecanismo útil para que o \textit{site} forneça um bom tempo de resposta para os usuários num momento em que o mesmo esteja recebendo muitas visitas.

\begin{figure}[hbt]
\centering
\includegraphics[angle=90, totalheight=0.8\textheight]{imagens/benchmark.png}
\caption{Resultados do teste de carga realizado para verificar a eficiência do mecanismo de \textit{View Caching}.}
\label{fig:benchmark}
\end{figure}

\section{\textit{View Caching} Distribuído}
Originalmente, o mecanismo de \textit{View Caching} armazena as páginas num diretório do servidor que está tratando a requisição, sem qualquer relação com os outros servidores que possam estar participando do balanceamento de carga.
Ou seja, esse diretório não é compartilhado entre eles, e cada servidor tem seu próprio diretório.
Portanto, se, por exemplo, duas requisições da página de depoimentos são feitas, e essa página não estiver em \textit{cache} em nenhum servidor, e cada uma for tratada por um servidor diferente, então as duas passarão, desnecessariamente, por todo o ciclo de requisição do CakePHP, como apresentado na Figura~\ref{fig:view_caching_individual}.

\begin{figure}[hbt]
\centering
\includegraphics[width=\textwidth]{imagens/view_caching_individual.png}
\caption{Mecanismo de \textit{View Caching} não compartilhado entre os servidores.}
\label{fig:view_caching_individual}
\end{figure}

Pior acontece quando a limpeza do \textit{View Cache} é realizada somente em um dos servidores.
Por exemplo, suponhamos que dois servidores estejam com a página de depoimentos em \textit{cache}.
Se um novo depoimento é cadastrado, as páginas relacionadas à entidade ``Depoimento'' que estão armazenadas no \textit{View Cache} devem ser excluídas, de maneira a serem geradas com os novos dados, como explicado na Seção~\ref{sec:view_caching}.
Contudo, como são dois servidores, somente as páginas do \textit{View Cache} do servidor que tratou a requisição de cadastro serão excluídas.
Se, nesse momento, o outro servidor receber uma requisição da página de depoimentos, ele irá retornar a versão que está em seu \textit{View Cache}, desatualizada.

Para resolver esses problemas, deve existir um \textit{View Cache} único que todos os servidores usarão.
Neste trabalho, será utilizado um sistema distribuído de \textit{cache} de objetos na memória: Memcached\footnote{\url{http://memcached.org/}}.
Os objetos, nesse caso, serão as páginas do \textit{View Cache}.
O Memcached é mais um sistema de armazenamento de chave-valor na memória, contudo, ele consegue utilizar a memória de todos os servidores como se fosse uma só, como pode ser visto na Figura~\ref{fig:memcached}.
Se, ao invés de 2, houvessem cinquenta servidores Web, ainda haveria 64MB de tamanho útil no primeiro quadro da figura, e aproximadamente 3.2GB no segundo.

No CakePHP, o \textit{View Cache} só consegue ser armazenado em arquivos, sendo necessário alterar algumas linha de código do \textit{framework}, que dizem respeito à escrita e leitura dos mesmos, para ter essa funcionalidade.
Ao final, obtém-se um sistema que pode ser facilmente incrementado com novos servidores, de acordo com a necessidade e com o uso de \textit{View Cache}.

\begin{figure}[hbt]
\centering
\includegraphics[scale=0.5]{imagens/memcached.png}
\caption{Utilização do Memcached.}
\label{fig:memcached}
\end{figure}
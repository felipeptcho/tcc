\mychapter{Especificação do Sistema}{chp:especificacao}

Neste capítulo trata-se de detalhar o aspecto e as principais funcionalidades do \textit{site} Kuase~de~Graça.
Todo o desenvolvimento é baseado nas especificações aqui descritas.

\section{Cadastro de Usuários e Autenticação}
O Kuase~de~Graça é um sistema multiusuário, ou seja, vários usuários podem estar acessando-o num certo momento.
Para atender determinadas solicitações, o sistema precisa identificar o usuário que fez a requisição.
Por exemplo, ao receber da Internet uma solicitação de compra de trinta lances, é necessário saber quem fez o pedido, de maneira que os lances sejam atribuídos ao usuário correto.
Por esse motivo, para participar das principais atividades do \textit{site}, os usuários precisarão realizar um cadastro, informando, entre outros dados, um \textit{e-mail} e uma senha, de forma que o sistema possa identificá-los corretamente através do processo de autenticação (Figura~\ref{fig:login}).

\begin{figure}[hbt]
\centering
\includegraphics[width=0.8\textwidth]{imagens/login.png}
\caption{Processo de autenticação de um usuário quando ele requisita uma página restrita a usuários autenticados.}
\label{fig:login}
\end{figure}

O processo de autenticação consiste em determinar se alguém é realmente quem ele alega ser. % Colocar referência.
Sempre que necessário, o sistema irá solicitar informações que, teoricamente, só o usuário verdadeiro pode fornecer.
Essas informações geralmente são um \textit{login} e uma senha.
O \textit{login} serve para identificar o usuário em meio a outros.
A senha, informada corretamente, prova que o usuário é quem ele diz ser\footnote{É assumido que o conhecimento da senha garante a autenticidade desse usuário.}.

Cada usuário deve possuir um \textit{login} único no sistema.
No Kuase~de~Graça, o campo de \textit{e-mail} informado no cadastro será usado como o \textit{login}.
Ele é suficiente para isso, pois não será permitido que dois usuários sejam cadastrados com o mesmo \textit{e-mail} no sistema.

Além do \textit{e-mail} e senha, o formulário de cadastro requisitará que o usuário aceite os termos de uso (Seção~\ref{sec:termos}) e preencha alguns dados pessoais, bem como o endereço para onde devem ser enviados os produtos que ele arrematar.
Todos os campos que devem ser informados pelo usuário podem ser divididos em 3 grupos:

\begin{itemize}
\item \textbf{Dados Gerais}:
\begin{itemize}
\item Nome Completo;
\item Sexo;
\item Telefone;
\item Data de Nascimento;
\item CPF;
\end{itemize}

\item \textbf{Dados para identificação no sistema}
\begin{itemize}
\item Nome de Usuário;
\item \textit{E-mail};
\item Senha;
\end{itemize}

\item \textbf{Dados para Entrega de Produtos}
\begin{itemize}
\item CEP;
\item Endereço;
\item Número;
\item Complemento (opcional);
\item Bairro;
\item Cidade;
\item Estado.
\end{itemize}
\end{itemize}

O campo ``Nome de Usuário'' é o nome que irá aparecer nos leilões em que esse usuário fizer uma oferta (item I da Figura~\ref{fig:leilao}).
Ele deve ter entre quatro e quinze caracteres.
É obrigatório que esses caracteres sejam alfanuméricos ou \textit{underscores} (\_).

Após a submissão do formulário, o usuário terá sua conta no Kuase~de~Graça criada.
Ela conterá inicialmente 5 lances, contudo, antes de poder acessá-la, ele deverá ativar seu cadastro, conforme explicado na Seção~\ref{sec:ativacao_cadastro}.

\section{Redefinição de Senha}
Se ocorrer de um usuário esquecer a sua senha, ele ficará impossibilitado de acessar o sistema.
Para reverter essa situação, ele poderá solicitar a redefinição de sua senha.
O sistema pedirá que ele se identifique, informando seu \textit{login}.
Se o \textit{login} informado realmente estiver cadastrado, uma mensagem será enviada para o endereço de \textit{e-mail} desse usuário com um \textit{link} que, quando acessado, permitirá que ele crie uma nova senha.
Esse \textit{link} deve ser único, de uma forma que não possa ser reproduzido por usuários mal intencionados.
Ele também deve ser invalidado após a nova senha ter sido criada ou após três dias da solicitação.

Mesmo que não tenham esquecido a senha, os usuários poderão solicitar a alteração da mesma sem terem que receber um \textit{e-mail} com o \textit{link}.
Basta que eles informem a senha atual e a nova senha desejada.

\section{Ativação de Cadastro}
\label{sec:ativacao_cadastro}
É necessário garantir que um usuário que está se cadastrando é capaz de receber mensagens no endereço de \textit{e-mail} informado.
Se, por exemplo, o \textit{e-mail} não existir e o usuário precisar redefinir sua senha, ele não conseguirá concluir o processo, pois não receberá a mensagem com o \textit{link} para a criação da nova senha.

Para evitar esse problema, logo após o cadastro, uma mensagem de confirmação será enviada para o endereço de \textit{e-mail} informado.
Ela conterá um \textit{link} que o usuário deve acessar.
Isso irá confirmar ao sistema que o usuário é capaz de receber mensagens.
Somente após essa confirmação, o usuário terá sua conta ativada e poderá acessá-la.

\section{Convite de Usuários}
O sistema deve fornecer meios pelos quais os usuários possam facilmente convidar seus amigos para participarem do \textit{site}.
Após a ativação de seu cadastro, o usuário receberá acesso a um \textit{link} pessoal que ele poderá distribuir entre seus contatos.
Todos os amigos que acessarem esse \textit{link} e se cadastrarem (e realizarem a ativação do cadastro) serão considerados convidados desse usuário e poderão ser visualizados por ele numa lista específica.
Essa lista estará acessível através da página ``Minha Conta'' (Seção~\ref{sec:minha_conta}).
Para cada convidado que realizar sua primeira compra de lances, o usuário que convidou receberá uma recompensa de dez lances.

Integradas ao sistema, devem estar ferramentas que os usuários poderão utilizar para distribuir seus \textit{links} pessoais de convite nas redes sociais e via \textit{e-mail} com facilidade.

\section{Uso de CAPTCHA}
\label{sec:captcha}
CAPTCHA (\textit{Completely Automated Public Turing Test to Tell Computers and Humans Apart}) é o mecanismo que usa imagens geradas aleatoriamente, contendo palavras ou caracteres, com o intuito de verificar se um usuário é humano \citep{vonAhn:2004:THC:966389.966390}.
Um exemplo de CAPTCHA com caracteres pode ser visto na Figura~\ref{fig:captcha}.
Esse mecanismo é usado, por exemplo, em alguns formulários que possuem certo nível de vulnerabilidade a programas de computador maliciosamente implementados para executar uma determinada tarefa diversas vezes por segundo.

No caso do Kuase~de~Graça, um usuário mal intencionado poderia programar um algoritmo para submeter repetidas vezes o formulário de \textit{login} do \textit{site}, utilizando várias combinações de senha, no intuito de descobrir a senha de outro usuário e poder se autenticar como ele no sistema.
Por evitar isso, no momento do \textit{login}, um usuário terá 5 tentativas para acertar sua senha.
Se não acertar, ele poderá fazer novas tentativas, contudo, digitando corretamente a sequência de caracteres ou palavras do CAPTCHA, provando não ser um algoritmo malicioso.

\begin{figure}[hbt]
\centering
\includegraphics[scale=1]{imagens/captcha.jpg}
\caption{Exemplo de CAPTCHA.}
\label{fig:captcha}
\end{figure}

\section{Compra de Lances}
\label{sec:compra_lances}
Inicialmente, a conta de cada usuário possui apenas 5 lances.
Mais lances podem ser adquiridos através de uma página de compra.
Os lances não poderão ser comprados individualmente, mas serão vendidos em ``pacotes''.
Cada pacote possui uma quantidade de lances e preço específicos.
Após selecionar o pacote desejado, o usuário será redirecionado a uma próxima página, onde poderá escolher a forma de pagamento desejada e dar as informações financeiras necessárias.
Se o pagamento for aprovado, os lances são adicionados à sua conta no Kuase~de~Graça.

O usuário ainda poderá visualizar numa página, o andamento de todos os pagamentos feitos para o Kuase~de~Graça.

O processo de pagamento deverá ficar por conta de uma solução para pagamentos online, o PagSeguro\footnote{\url{https://pagseguro.uol.com.br/}}.
Desse modo, além de contar com várias formas de pagamento, o usuário não precisará se preocupar com o sigilo de seus dados financeiros, pois eles não terão que ser informados diretamente ao Kuase~de~Graça.
E ainda contará com a garantia de que o seu dinheiro será devolvido na ocasião de algum problema.
Além disso, a adição de novas formas de pagamento é feita pelo PagSeguro de maneira abstrata para o Kuase~de~Graça, e os pagamentos feitos a prazo pelos clientes são recebidos de uma só vez pelo \textit{site}.

\section{Códigos Promocionais}
\label{sec:codigos_promocionais}
O sistema deverá fornecer um meio de gerar códigos (ou cupons) de desconto para os usuários utilizarem na compra de pacotes de lances.
Esses códigos podem ser digitados pelos usuários no momento da compra ou serem enviados por \textit{e-mail} na forma de \textit{links}.
Após a utilização de um código de desconto, o mesmo deverá ser invalidado, evitando o seu reuso.
Somente se a transação de pagamento não for aprovada pela instituição financeira, o código poderá voltar a ser usado pelo usuário para uma nova tentativa.

\section{Depoimentos}
\label{sec:depoimentos}
Quando um usuário receber em sua residência um produto que ele arrematou no Kuase~de~Graça, ele poderá enviar um depoimento por \textit{e-mail}.
Em troca, ele receberá vinte lances.
Um depoimento consiste numa foto do usuário com o produto e num texto que conte a experiência que ele teve no \textit{site}.

Uma página deverá reunir todos os depoimentos recebidos dos usuários, ordenados pela data do recebimento.

\section{\textit{Layout} das Páginas}
O \textit{layout} padrão para as páginas do \textit{site} deve seguir o formato apresentado na Figura~\ref{fig:layout}.

\begin{figure}[hbt]
\centering
\includegraphics[width=0.5\textwidth]{imagens/layout.png}
\caption{\textit{Layout} padrão para as páginas do Kuase~de~Graça.}
\label{fig:layout}
\end{figure}

\begin{itemize}
\item
O item A é o cabeçalho do \textit{site}.
Ele deve conter a logomarca e um formulário (\textit{e-mail} e senha) para \textit{login} dos usuários.
Se o usuário já estiver autenticado no sistema, ao invés do formulário de \textit{login}, % Explicar o que é login.
a quantidade de lances atual dele deve ser mostrada em destaque, junto com o nome de usuário, % Explicar o que é um nome de usuário.
um \textit{link} para a realização do \textit{logout}, % Explicar o que é logout.
outro para a página de compra de lances e outro para a página ``Minha Conta'' (Seção~\ref{sec:minha_conta}),
conforme a Figura~\ref{fig:logged}.
Se a quantidade de lances do usuário for menor que cinquenta, ela deve ser mostrada na cor vermelha, com o objetivo de alertar o usuário da baixa quantidade;

\item
O item B é o conteúdo específico da página;

\item
O item C é um espaço para mostrar no máximo 2 leilões atualmente em destaque % Explicar o que é um leilão em destaque.
no \textit{site}, ordenados por data e hora de início dos mesmos;

\item
O item D é um espaço para \textit{plug-ins} de redes sociais.
Pode ser vista na Figura~\ref{fig:plugins}, por exemplo, a aparência de um \textit{plug-in} do Twitter\footnote{\url{http://twitter.com/}} que mostra as últimas atualizações de um determinado perfil, e do \textit{plug-in} \textit{Like Box}\footnote{\url{http://developers.facebook.com/docs/reference/plugins/like-box/}} do Facebook\footnote{\url{http://www.facebook.com/}}, que mostra a quantidade de pessoas que ``curtiram'' uma dada página dessa rede;

\item
O item E é o rodapé do \textit{site}, que deve conter uma lista de \textit{links} para diversas páginas do \textit{site}.
\end{itemize}

\begin{figure}[hbt]
\centering
\includegraphics[width=0.3\textwidth]{imagens/logged.png}
\caption{Informações mostradas no cabeçalho do \textit{site} quando o usuário está autenticado.} % Inserir legenda
\label{fig:logged}
\end{figure}

\begin{figure}[hbt]
\centering
\includegraphics[width=0.8\textwidth]{imagens/social_plugins.png}
\caption{Um \textit{plug-in} do Twitter à esquerda e um do Facebook à direita.}
\label{fig:plugins}
\end{figure}

\section{Página Inicial}\label{sec:pagina_inicial}
O conteúdo da página inicial deve ser composto por no máximo 8 leilões que ainda vão começar ou que estão em execução, ordenados por data e hora de início.
Se não existir leilões cadastrados no sistema, uma mensagem explicativa deve ser mostrada.
Cada leilão deve seguir o formato apresentado na Figura~\ref{fig:leilao}.

\section{Detalhes de um Leilão}
O espaço para cada leilão na página inicial é limitado e nem todas as informações do mesmo podem ser exibidas, por isso todo leilão deverá ter uma página específica onde o usuário poderá visualizar o histórico dos dez últimos usuários que fizeram uma oferta e os detalhes do produto que está sendo leiloado (mais imagens e informações técnicas), conforme a Figura~\ref{fig:detalhes}.

\begin{figure}[hbt]
\centering
\includegraphics[width=0.8\textwidth]{imagens/detalhes_leilao.png}
\caption{Página de detalhes de um leilão.}
\label{fig:detalhes}
\end{figure}

\section{Leilões Arrematados}
A página inicial exibe somente os leilões que vão iniciar ou que estão em execução (Seção~\ref{sec:pagina_inicial}).
Os leilões que já foram arrematados devem ser exibidos numa página específica, ordenados por data e hora de arremate, conforme a Figura~\ref{fig:arrematado}.
Os leilões que finalizaram sem um arrematante não devem aparecer nessa página.

\begin{figure}[hbt]
\centering
\includegraphics[width=\textwidth]{imagens/leilao_arrematado.png}
\caption{Aspecto de um leilão na página de leilões arrematados.}
\label{fig:arrematado}
\end{figure}

\section{Página ``Minha Conta''}
\label{sec:minha_conta}
É onde o usuário deverá ter acesso a informações pertinentes à sua conta no Kuase~de~Graça.
Ele poderá:

\begin{itemize}
\item
enviar convites para seus contatos;

\item
visualizar seus convidados;

\item
visualizar o extrato de seus lances;

\item
adquirir mais lances;

\item
visualizar leilões que ele participou;

\item
visualizar leilões que ele arrematou;

\item
realizar o pagamento dos leilões arrematados;

\item
visualizar seus pagamentos;

\item
alterar seus dados de cadastro.

\end{itemize}

\section{Página ``Fale Conosco''}
Trata-se de uma página que os usuários podem usar para entrar em contato com a equipe de atendimento do Kuase~de~Graça.
Essa página deve conter um formulário para o preenchimento do nome, \textit{e-mail}, assunto e mensagem do usuário.
Quando o formulário for submetido, esses campos devem ser enviados por \textit{e-mail} para a equipe de atendimento do \textit{site}.

\section{Simulador}
Os leilões virtuais são novidade para muitas pessoas.
Por isso, é necessário a existência de uma página para demonstrar como eles funcionam.
Através de um simulador, os usuários podem participar de um leilão fictício e tirar a maioria de suas dúvidas por conta própria.
O simulador deve:
\begin{itemize}
\item
permitir que os usuários escolham que tipo de leilão eles desejam simular (progressivo ou regressivo);

\item
explicar as partes de um leilão como mostrado na Figura~\ref{fig:leilao};

\item
explicar como funciona o tipo de leilão escolhido (Seção~\ref{sec:tipos_leilao});

\item
iniciar o leilão e instruir o usuário fazer uma oferta.
\end{itemize}

Quando o leilão terminar, o simulador deve explicar por quanto o produto foi arrematado e como o usuário deveria proceder para recebê-lo em sua residência.

\section{Termos de Uso}
\label{sec:termos}
São as regras de utilização do \textit{site}.
Especificam quem pode se cadastrar, o funcionamento dos leilões, o procedimento para pagamento e entrega dos produtos arrematados e as responsabilidades do \textit{site} para com seus usuários.
Elas devem ser mostradas no momento do cadastro e todo usuário deve concordar com as mesmas para poder criar sua conta.
Também deve existir uma página específica contendo os termos de uso para que os usuários possam consultar posteriormente.

\section{Administração}
\label{sec:administracao}
O sistema deve possuir uma área administrativa designada para usuários com a devida autorização.
Essa área deve permitir:

\begin{itemize}
\item
Cadastrar, editar e excluir códigos promocionais (Seção~\ref{sec:codigos_promocionais});

\item
Cadastrar, editar e excluir os depoimentos enviados por usuários que receberam produtos arrematados (Seção~\ref{sec:depoimentos});

\item
Cadastrar, editar e excluir os produtos do \textit{site};

\item
Cadastrar, editar e excluir os leilões. O cadastro dos leilões deve ser feito a partir dos produtos;

\item
Reiniciar um determinado leilão, devolvendo todos os lances dos participantes e o agendando para uma nova data.
Essa funcionalidade pode ser útil se houver algum problema na execução do leilão que o invalide.
Por exemplo, se o \textit{site} saísse do ar, ficaria impossibilitado de receber ofertas e, consequentemente, os leilões em execução seriam finalizados, sem que os participantes tivessem oportunidade ``cobrir'' a última oferta computada;

\item
Cadastrar, editar e excluir os pacotes de lances a serem vendidos na página de compra de lances (Seção~\ref{sec:compra_lances}).
\end{itemize}

Os usuários que têm acesso à área administrativa são semelhantes a usuários clientes, contudo, eles não podem fazer ofertas nos leilões do \textit{site}.

\section{Casos de Uso do Sistema}
\label{sec:casos_de_uso}
De acordo com a Seção~\ref{sec:administracao}, existem dois tipos de usuário no sistema: usuário cliente e administrador.
É apresentado na Figura~\ref{fig:casos_de_uso_usuario}, o diagrama de casos de uso de um usuário genérico do sistema (cliente ou administrador).

O diagrama de casos de uso para um usuário cliente do sistema, que não tem privilégios administrativos, pode ser visto na Figura~\ref{fig:casos_de_uso_usuario_cliente}.
Pode-se notar pelo diagrama que ele herda os casos de uso do usuário genérico e tem a capacidade de fazer ofertas.

Já o diagrama de casos de uso para um usuário que é administrador do sistema, é apresentado na Figura~\ref{fig:casos_de_uso_usuario_administrador}.
Ele também herda os casos de uso do usuário genérico, mas ao contrário do cliente, não pode participar dos leilões.

\begin{figure}[hbt]
\centering
\includegraphics[angle=90, totalheight=0.9\textheight]{imagens/casos_de_uso_usuario.png}
\caption{Diagrama de casos de uso de um usuário genérico do sistema.}
\label{fig:casos_de_uso_usuario}
\end{figure}

\begin{figure}[hbt]
\centering
\includegraphics[width=0.5\textwidth]{imagens/casos_de_uso_usuario_cliente.png}
\caption{Diagrama de casos de uso de um usuário cliente do sistema.}
\label{fig:casos_de_uso_usuario_cliente}
\end{figure}

\begin{figure}[hbt]
\centering
\includegraphics[angle=90, totalheight=0.8\textheight]{imagens/casos_de_uso_usuario_administrador.png}
\caption{Diagrama de casos de uso de um usuário administrador do sistema.}
\label{fig:casos_de_uso_usuario_administrador}
\end{figure}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Classe de documento LATEX para confecÃ§Ã£o de trabalhos de conclusÃ£o de      %%
%% curso e dissertaÃ§Ãµes de mestrado do Departamento de Tecnologia da          %%
%% InformaÃ§Ã£o da Universidade Federal de Alagoas                              %%
%%                                                                            %%
%% Autores:                                                                   %%
%%       Rodrigo Paes - rbp@les.inf.puc-rio.br                                %%
%%       Hyggo Almeida - hyggo@dee.ufcg.edu.br                                %%
%%                                                                            %%
%% Data: 19 de fevereiro de 2004                                              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% ModificaÃ§Ãµes:                                                              %%
%% Fontes (lettrine), layout inicial dos capÃ­tulos (adaptado de B. Lopes),    %%
%% hyperlinks, annotations com abstract das referÃªncias no pdf (article).     %%
%%                                                                            %%
%% Adaptado por:                                                              %%
%%       Rian Pinheiro - rian.gabriel@gmail.com                               %%
%%       Ivan Martins - ivn.martins@gmail.com                                 %%
%%                                                                            %%
%% Data: Outubro de 2010                                                      %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



\NeedsTeXFormat{LaTeX2e}
   \ProvidesClass{ic}[2004/02/18 v1.0 Modelo IC-UFAL de Trabalhos de Conclusao 
de Curso e Dissertacoes]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% DefiniÃ§Ã£o de extensÃ£o da classe REPORT - Papel A4, FONTE 12pt              %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\LoadClass[a4paper,12pt]{report} %210 mm X 294 mm
%%------------------------------------------------------------------------------
%% Habilita a utilizaÃ§Ã£o de arroba
%%------------------------------------------------------------------------------
\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%      LISTA DE PACOTES REQUERIDOS PARA A UTILIZAÃ‡ÃƒO DESTA CLASSE            %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\RequirePackage[T1]{fontenc} % DefiniÃ§Ã£o da codificaÃ§Ã£o das fontes como T1
\RequirePackage{graphicx} % InclusÃ£o de imagens
\RequirePackage{setspace} % DefiniÃ§Ã£o dos espaÃ§amemtos entre linhas
\RequirePackage{calc} % AceitaÃ§Ã£o de notaÃ§Ã£o infixa
\RequirePackage{fancyhdr} % PersonalizaÃ§Ã£o de cabeÃ§alhos e rodapÃ©s
\RequirePackage{listings} % InclusÃ£o de cÃ³digo fonte de diversas linguagens
\RequirePackage{pslatex} % UtilizaÃ§Ã£o de fontes PostScript Tipo 1
\RequirePackage[utf8]{inputenc} % Uso caracteres especiais UTF8
\RequirePackage[a4paper]{geometry} % Para ajustar o papel
\RequirePackage[portuguese,english]{babel} % Para habilitar os macros de linguas
\RequirePackage{natbib} % Estilo das referÃªncias
\RequirePackage[hyphens]{url} % Para lidar com URLs
\RequirePackage{doc} % ContÃ©m a definiÃ§Ã£o da logomarca do BibTex
\RequirePackage{color,colortbl} % Permite "pintar" as tabelas
\RequirePackage[active]{srcltx} % Busca inversa do dvi para o tex
\RequirePackage{ifthen} % UtilizaÃ§Ã£o de condicionais em latex

%%%%%%%%%%%%%%%%%%%%%%%%%% [Rian,Ivan] InÃ­cio ModificaÃ§Ãµes %%%%%%%%%%%%%%%%%%%%%
\RequirePackage{amsmath} %
\RequirePackage{bbm} 
\RequirePackage{amssymb} %
\RequirePackage{amscd} %
\RequirePackage{units} %
\RequirePackage{undertilde} % UtilizaÃ§Ã£o de caracteres com til em baixo

\RequirePackage[ruled,chapter]{algorithm}
\RequirePackage{algpseudocode}
\floatname{algorithm}{Algoritmo}
\renewcommand{\listalgorithmname}{Lista de Algoritmos}
\newenvironment{myequation}[2]
{\begin{equation}%
 \label{#1}%
\addcontentsline{loe}{table}{\theequation~ #2}}
{\end{equation}}


%%------------------------------------------------------------------------------
%% Fontes
%%------------------------------------------------------------------------------
\RequirePackage{fourier}  \newcommand\NomeFonte{Fourier-GUT\textit{enberg}}
\RequirePackage[euler-digits]{eulervm}  \newcommand\NomeFonteMat{Euler Virtual Math}
\RequirePackage{eucal} 
\RequirePackage{lettrine} %
\newcommand{\maior}{\text{\large \textgreater~}}
\newcommand{\menor}{\text{\large \textless~}}

%\RequirePackage{yfonts} %

\RequirePackage{tikz} % Pacote para desenhos (fig:diagrama)
\RequirePackage{xcolor} % DefiniÃ§Ã£o das cores
\RequirePackage{bm} % Pacote para criaÃ§Ã£o do modelo de RSsF (fig:model_rssf)
\RequirePackage{float} %
\RequirePackage{pdfcomment} % ComentÃ¡rio do Abstract
\RequirePackage{hyperref} %

\RequirePackage{subfig} %
%\RequirePackage{booktabs} %

%\definecolor{light_gray}{gray}{0.95}
\definecolor{Red}{rgb}{0.5,0,0}
\definecolor{Blue}{rgb}{0,0,0.5}

\newcommand\NomeFonteCap{Art Nouveau Caps}

\input{ArtNouvc.fd}
\newcommand*\myfont{\fontsize{60}{75}\usefont{U}{ArtNouvc}{xl}{n}}

%%------------------------------------------------------------------------------
%% Redefinindo capÃ­tulo
%%------------------------------------------------------------------------------
\newcounter{mychaptercounter}
\newenvironment{mychapterenviroment}{\refstepcounter{mychaptercounter}}{}
\newcommand{\mychapter}[2]{
\stepcounter{chapter}
\chapter*{
\begin{mychapterenviroment}
\label{#2}
\end{mychapterenviroment}
\begin{flushright}
\normalfont\myfont\arabic{mychaptercounter}\normalfont\Huge\bf\\
\vspace{1em}
#1
\end{flushright}
}
\addcontentsline{toc}{chapter}{\arabic{mychaptercounter}\hspace{1em}#1}
}

%%------------------------------------------------------------------------------
%% Comando para colocar uma inicializaÃ§Ã£o em cada capÃ­tulo
%%------------------------------------------------------------------------------
% \newcommand{\initialcitation}[4]{
% \begin{raggedleft}
% {\it``#1''\\\vspace{1em}{\em{\bf#2}\\#3 (#4)}\\}
% \end{raggedleft}
% \vspace{2em}
% }

%%------------------------------------------------------------------------------
%% Para a fonte da lettrine ser a que defini
%%------------------------------------------------------------------------------
\renewcommand{\LettrineFontHook}{\myfont}

%%------------------------------------------------------------------------------
%% OpÃ§Ãµes do pacote lettrine
%%------------------------------------------------------------------------------
\setcounter{DefaultLines}{3}
\renewcommand{\DefaultLoversize}{0}
\renewcommand{\DefaultLraise}{0.1}
\setlength{\DefaultNindent}{0em}
\setlength{\DefaultSlope}{-.5em}


% ComentÃ¡rio do abstract
\newcommand{\comAbstract}[3]{
\pdfmarkupcomment[color=white,author=#2,markup=Squiggly]{#1}{#3}
}
\newcommand{\doi}[1]{\textbf{DOI}~{\href{http://dx.doi.org/#1}{\color{Blue}#1}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%% [Rian,Ivan] Fim ModificaÃ§Ãµes %%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                DECLARAÃ‡ÃƒO DE OPÃ‡Ã•ES: DISSERTAÃ‡ÃƒO e TCC                     %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%------------------------------------------------------------------------------
%% Define TCC como padrÃ£o
%%------------------------------------------------------------------------------
\newboolean{tcc}
\setboolean{tcc}{true}

%%------------------------------------------------------------------------------
%% Declara a opÃ§Ã£o de dissertaÃ§Ã£o
%%------------------------------------------------------------------------------
\DeclareOption{dissertacao}{%
    \setboolean{tcc}{false}
    \typeout{Tipo de Documento: Dissertacao de Mestrado.}
}

%%------------------------------------------------------------------------------
%% Declara a opÃ§Ã£o de TCC
%%------------------------------------------------------------------------------
\DeclareOption{tcc}{%
    \setboolean{tcc}{true}
    \typeout{Tipo de Documento: Trabalho de Conclusao de Curso.}
}

%%------------------------------------------------------------------------------
%% Executa as opÃ§Ãµes
%%------------------------------------------------------------------------------
\ProcessOptions\relax


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                 DEFINIÃ‡ÃƒO DO LAYOUT DO DOCUMENTO                           %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%------------------------------------------------------------------------------
%%  TAMANHOS VERTICAIS - COPIADO DO ESTILO PADRÃƒO DA ABNT - Pacote abntex.cls
%%------------------------------------------------------------------------------
% A distancia entre o topo do cabeÃ§alho e o topo do texto Ã© de 1cm, 
% isto Ã©, 1cm=\headheight+\headsep
% ...mas, nÃ³s consideramos a profundidade do cabeÃ§allho, adicionando 2mm.
\setlength{\headsep}{1.2cm-\headheight}
% A distÃ¢ncia entre a borda do papel e o nÃºmero deve ser de 2cm
% 2cm=\topmargin+\voffset+1in
\setlength{\topmargin}{2cm-1in-\voffset}
% A borda inferior deve ser de 2cm
% \paperheight=\topmargin+\voffset+1in+\headheight+\headsep+\textheight+2cm
\setlength{\textheight}%
{\paperheight-\topmargin-\voffset-1in-\headheight-\headsep-2cm}

%%------------------------------------------------------------------------------
%% TAMANHOS HORIZONTAIS - COPIADO DO ESTILO PADRÃƒO DA ABNT - Pacote abntex.cls
%%------------------------------------------------------------------------------
% A margem esquerda Ã© de 3cm e a direita Ã© igual a 2cm.
\setlength{\oddsidemargin}{3cm-\hoffset-1in}
% para compatibilidade com impressÃ£o frente e verso, o tamanho das margens 
%deve ser alterado
\setlength{\evensidemargin}{2cm-\hoffset-1in}
% \paperwidth=\textwidth+\oddsidemargin+\hoffset+1in+2cm
\setlength{\textwidth}{\paperwidth-\oddsidemargin-\hoffset-1in-2cm}

%%------------------------------------------------------------------------------
%% Define o cabeÃ§alho das pÃ¡ginas
%%------------------------------------------------------------------------------
% Limpa qualquer configuraÃ§Ã£o anterior
\fancyhf{}

% Define a apariÃ§Ã£o do nome da seÃ§Ã£o no cabeÃ§alho(esquerda)
\lhead{\rightmark} 

% Define a apariÃ§Ã£o do nÃºmero da pÃ¡gina no cabeÃ§alho(direita)
\rhead{\thepage} % NÃºmero da pÃ¡gina

% Define a largura da barra
\renewcommand{\headrulewidth}{0.6pt} 

% Define a altura do cabeÃ§alho
\addtolength{\headheight}{3.05pt} 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                           DEFINIÃ‡ÃƒO DOS COMANDOS                           %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando CAPA
%%----------------------------------------------------------------------------
\newcommand{\capa}{%
    \thispagestyle{empty}%
    \begin{flushright} % Alinhamento Ã  direita
      \ifthenelse{\boolean{tcc}}{\href{http://ic.ufal.br}{\includegraphics[width=.4\linewidth]{IC.png}}}{\href{http://ic.ufal.br}{\includegraphics[width=.4\linewidth]{mestrado.png}}}\\%
% [Rian] Adicionado href
      \Large \vspace*{2cm} \ifthenelse{\boolean{tcc}}{Trabalho de Conclusão de 
Curso}{Dissertação de Mestrado}\\%
      \vspace*{2cm} \LARGE \textbf{\@titulo} \\%
      \vspace*{2cm}%
      \Large \href{\@autorPagina}{\color{black}{\@autorNome}} \\ \normalsize %
\texttt{\@autorEmail} \\ % [Rian] Adicionado href
      \ifthenelse{\boolean{autorDoisExiste}}{\Large \@autorDoisNome \\ %
\normalsize \texttt{\@autorDoisEmail}\\}{}%
      \vspace*{2cm}%
      \Large%
      \ifthenelse{\boolean{orientadorDoisExiste}}{Orientadores:}{Orientador:}%
      \\
      \vspace*{0.2cm}%
      \large
      \href{\@orientadorPagina}{\color{black}\@orientadorNome}\\%
% [Rian] Adicionado href
      \@orientadorDoisNome\\
      \@orientadorTresNome\\
      \normalsize \vfill Maceió, \@data%
    \end{flushright}%
    \folhaDeRosto%
    \aprovacao%
    \renewcommand{\thepage}{\roman{page}} \setcounter{page}{0}
    \normalsize%
}


%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando FOLHA DE ROSTO
%%------------------------------------------------------------------------------
\newcommand{\folhaDeRosto}{
    \newpage%
    \thispagestyle{empty}%
    \begin{center}
        \Large
        \@autorNome\\
        \@autorDoisNome\\
        \vspace*{3cm} \LARGE \textbf{\@titulo} \\%
    \end{center}
    \vspace*{2cm}
    \hspace{5 cm}%
    \large%
    \hfill%
    \begin{minipage}{ 11 cm }%
        \ifthenelse{\boolean{tcc}}%
       {Monografia apresentada como requisito parcial para obtenção do grau de
Bacharel em Ciência da Computação do Instituto de Computação da Universidade
Federal de Alagoas.}%
        {Dissertação apresentada como requisito parcial para obtenção do grau de
Mestre pelo Curso de Mestrado em Modelagem Computacional de Conhecimento do
Instituto de Computação da Universidade Federal de Alagoas.}%
    \end{minipage}%
    \begin{flushright}
        \vspace*{2cm} \large%
        \ifthenelse{\boolean{orientadorDoisExiste}}{Orientadores:}{Orientador:}%
        \\%
        \vspace*{0.5cm}%
        \Large%
        \@orientadorNome\\
        \vspace*{0.2cm}%
        \@orientadorDoisNome\\
        \vspace*{0.2cm}%
        \@orientadorTresNome\\
        \normalsize%
        \vfill%
        Maceió, \@data%
    \end{flushright}
    \normalsize%
}%

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando APROVACAO
%%------------------------------------------------------------------------------

%%------------------------------------------------------------------------------
%% Contador de assinaturas
%%------------------------------------------------------------------------------
\newcounter{numeroDeAssinaturas}
\setcounter{numeroDeAssinaturas}{2}

%%------------------------------------------------------------------------------
%% InÃ­cio do termo de aprovaÃ§Ã£o
%%------------------------------------------------------------------------------
\newcommand{\aprovacao}{
    \newpage%
    \thispagestyle{empty}%
    \ifthenelse{\boolean{tcc}}%
        {Monografia apresentada como requisito parcial para obtenção do grau de
Bacharel em Ciência da Computação do Instituto de Computação da Universidade
Federal de Alagoas, aprovada pela comissão examinadora que abaixo assina.}%
        {Dissertação apresentada como requisito parcial para obtenção do grau de
Mestre pelo Curso de Mestrado em Modelagem Computacional de Conhecimento do
Instituto de Computação da Universidade Federal de Alagoas, aprovada pela
comissão examinadora que abaixo assina.}%
    \begin{center}
        \normalsize%
        % 6 assinaturas: 0.7cm
        % 5 assinaturas: 1.5cm
        % 4 assinaturas: 3.0cm
        % 3 assinaturas: 4.0cm
        % 2 assinaturas: 4.0cm
        \ifthenelse{\value{numeroDeAssinaturas} = 2}{\vspace*{4cm}}{}%
        \ifthenelse{\value{numeroDeAssinaturas} = 3}{\vspace*{4cm}}{}%
        \ifthenelse{\value{numeroDeAssinaturas} = 4}{\vspace*{3cm}}{}%
        \ifthenelse{\value{numeroDeAssinaturas} = 5}{\vspace*{1.5cm}}{}%
        \ifthenelse{\value{numeroDeAssinaturas} = 6}{\vspace*{0.7cm}}{}%
        \rule{9cm}{0.02cm}\\%
        \vspace*{0.2cm}%
        \@orientadorNome~-~Orientador\\
        \@orientadorDepartamento\\
        \@orientadorInstituicao\\
        \ifthenelse{\boolean{orientadorDoisExiste}}{
            \vspace*{1cm}%
            \rule{9cm}{0.02cm}\\%
            \vspace*{0.2cm}%
            \@orientadorDoisNome~-~Orientador\\
            \@orientadorDoisDepartamento\\
            \@orientadorDoisInstituicao\\
        }{}
        \ifthenelse{\boolean{orientadorTresExiste}}{
            \vspace*{1cm}%
            \rule{9cm}{0.02cm}\\%
            \vspace*{0.2cm}%
            \@orientadorTresNome~-~Orientador\\
            \@orientadorTresDepartamento\\
            \@orientadorTresInstituicao\\
        }{}
        \vspace*{1cm}%
        \rule{9cm}{0.02cm}\\%
        \vspace*{0.2cm}%
        \@examinadorNome~-~Examinador\\
        \@examinadorDepartamento\\
        \@examinadorInstituicao\\
        \ifthenelse{\boolean{examinadorDoisExiste}}{
            \vspace*{1cm}%
            \rule{9cm}{0.02cm}\\%
            \vspace*{0.2cm}%
            \@examinadorDoisNome~-~Examinador\\
            \@examinadorDoisDepartamento\\
            \@examinadorDoisInstituicao\\
        }{}
        \ifthenelse{\boolean{examinadorTresExiste}}{
            \vspace*{1cm}%
            \rule{9cm}{0.02cm}\\%
            \vspace*{0.2cm}%
            \@examinadorTresNome~-~Examinador\\
            \@examinadorTresDepartamento\\
            \@examinadorTresInstituicao\\
        }{}
        \vfill%
        Maceió, \@data%
    \end{center}
    \normalsize%
}

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando INICIO para formataÃ§Ã£o de pÃ¡ginas
%%------------------------------------------------------------------------------
\newcommand{\inicio}{%
    \newpage%
    %NÃºmeros no estilo arÃ¡bico
    \renewcommand{\thepage}{\arabic{page}}%
    %Contador de pÃ¡gina = 1
    \setcounter{page}{1}%
    %EspaÃ§amento 1,5
    \onehalfspacing%
}%

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando AUTOR
%%------------------------------------------------------------------------------
\newcommand{\@autorNome}{}
\newcommand{\@autorEmail}{}
\newcommand{\@autorPagina}{}
\newcommand{\autor}[3]{
    \renewcommand{\@autorNome}{#1}
    \renewcommand{\@autorEmail}{#2}
    \renewcommand{\@autorPagina}{#3} % [Rian] pÃ¡gina Web do autor
}

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando AUTOR2
%%------------------------------------------------------------------------------
\newcommand{\@autorDoisNome}{~}
\newcommand{\@autorDoisEmail}{~}
\newboolean{autorDoisExiste}
\setboolean{autorDoisExiste}{false}
\newcommand{\autorDois}[2]{
    \renewcommand{\@autorDoisNome}{#1}
    \renewcommand{\@autorDoisEmail}{#2}
    \setboolean{autorDoisExiste}{true}
}

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando ORIENTADOR
%%------------------------------------------------------------------------------
\newcommand{\@orientadorNome}{}
\newcommand{\@orientadorPagina}{}
\newcommand{\@orientadorDepartamento}{}
\newcommand{\@orientadorInstituicao}{}
\newcommand{\orientador}[4]{
    \renewcommand{\@orientadorNome}{#1}
    \renewcommand{\@orientadorPagina}{#2} % [Rian] pÃ¡gina Web do orientador
    \renewcommand{\@orientadorDepartamento}{#3}
    \renewcommand{\@orientadorInstituicao}{#4}
}

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando ORIENTADOR 2
%%------------------------------------------------------------------------------
\newcommand{\@orientadorDoisNome}{~}
\newcommand{\@orientadorDoisEmail}{~}
\newcommand{\@orientadorDoisDepartamento}{~}
\newcommand{\@orientadorDoisInstituicao}{~}
\newboolean{orientadorDoisExiste}
\setboolean{orientadorDoisExiste}{false}
\newcommand{\orientadorDois}[4]{
    \renewcommand{\@orientadorDoisNome}{#1}
    \renewcommand{\@orientadorDoisEmail}{#2}
    \renewcommand{\@orientadorDoisDepartamento}{#3}
    \renewcommand{\@orientadorDoisInstituicao}{#4}
    \setboolean{orientadorDoisExiste}{true}
    \addtocounter{numeroDeAssinaturas}{1}
}

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando ORIENTADOR 3
%%------------------------------------------------------------------------------
\newcommand{\@orientadorTresNome}{~}
\newcommand{\@orientadorTresEmail}{~}
\newcommand{\@orientadorTresDepartamento}{~}
\newcommand{\@orientadorTresInstituicao}{~}
\newboolean{orientadorTresExiste}
\setboolean{orientadorTresExiste}{false}
\newcommand{\orientadorTres}[4]{
    \renewcommand{\@orientadorTresNome}{#1}
    \renewcommand{\@orientadorTresEmail}{#2}
    \renewcommand{\@orientadorTresDepartamento}{#3}
    \renewcommand{\@orientadorTresInstituicao}{#4}
    \setboolean{orientadorTresExiste}{true}
    \addtocounter{numeroDeAssinaturas}{1}
}

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando EXAMINADOR
%%------------------------------------------------------------------------------
\newcommand{\@examinadorNome}{}
\newcommand{\@examinadorEmail}{}
\newcommand{\@examinadorDepartamento}{}
\newcommand{\@examinadorInstituicao}{}
\newcommand{\examinador}[4]{
    \renewcommand{\@examinadorNome}{#1}
    \renewcommand{\@examinadorEmail}{#2}
    \renewcommand{\@examinadorDepartamento}{#3}
    \renewcommand{\@examinadorInstituicao}{#4}
}

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando EXAMINADOR 2
%%------------------------------------------------------------------------------
\newcommand{\@examinadorDoisNome}{~}
\newcommand{\@examinadorDoisEmail}{~}
\newcommand{\@examinadorDoisDepartamento}{~}
\newcommand{\@examinadorDoisInstituicao}{~}
\newboolean{examinadorDoisExiste}
\setboolean{examinadorDoisExiste}{false}
\newcommand{\examinadorDois}[4]{
    \renewcommand{\@examinadorDoisNome}{#1}
    \renewcommand{\@examinadorDoisEmail}{#2}
    \renewcommand{\@examinadorDoisDepartamento}{#3}
    \renewcommand{\@examinadorDoisInstituicao}{#4}
    \setboolean{examinadorDoisExiste}{true}
    \addtocounter{numeroDeAssinaturas}{1}
}

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando EXAMINADOR 3
%%------------------------------------------------------------------------------
\newcommand{\@examinadorTresNome}{~}
\newcommand{\@examinadorTresEmail}{~}
\newcommand{\@examinadorTresDepartamento}{~}
\newcommand{\@examinadorTresInstituicao}{~}
\newboolean{examinadorTresExiste}
\setboolean{examinadorTresExiste}{false}
\newcommand{\examinadorTres}[4]{
    \renewcommand{\@examinadorTresNome}{#1}
    \renewcommand{\@examinadorTresEmail}{#2}
    \renewcommand{\@examinadorTresDepartamento}{#3}
    \renewcommand{\@examinadorTresInstituicao}{#4}
    \setboolean{examinadorTresExiste}{true}
    \addtocounter{numeroDeAssinaturas}{1}
}

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando TITULO
%%------------------------------------------------------------------------------
\newcommand{\@titulo}{}
\newcommand{\titulo}[1]{\renewcommand{\@titulo}{#1}}

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do comando DATA
%%------------------------------------------------------------------------------
\newcommand{\@data}{}
\newcommand{\dataMesAno}[3]{
    \renewcommand{\@data}{
        #1 de #2
    }%%Fim de @DATA
}%Fim de DATA

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                       DEFINIÃ‡ÃƒO DE AMBIENTES                               %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do ambiente RESUMO
%%------------------------------------------------------------------------------
\newenvironment{resumo}{
    \onehalfspacing
    \chapter*{\centering Resumo}
}{}

%%------------------------------------------------------------------------------
%% RedefiniÃ§Ã£o do ambiente ABSTRACT
%%------------------------------------------------------------------------------
\renewenvironment{abstract}{
    \onehalfspacing
    \chapter*{\centering Abstract}
}{}

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do ambiente AGRADECIMENTOS
%%------------------------------------------------------------------------------
\newenvironment{agradecimentos}{
    \onehalfspacing
    \chapter*{\centering Agradecimentos}
}{}

%%------------------------------------------------------------------------------
%% DefiniÃ§Ã£o do ambiente CODIGO
%%------------------------------------------------------------------------------

% Ambiente COPIADO do pacote listings.sty (Ambiente:lstlisting)
\lstnewenvironment{codigo}[2][]
    {\lst@TestEOLChar{#2}
     \lstset{#1}
     \csname\@lst @SetFirstNumber\endcsname}
    {\csname\@lst @SaveFirstNumber\endcsname}

% ConfiguraÃ§Ãµes de exibiÃ§Ã£o
\lstset{numbers=left,  language=c++, frame=trbl}
\lstset{commentstyle=\textit}
\renewcommand{\lstlistlistingname}{Lista de Códigos}
\renewcommand{\lstlistingname}{Código}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                 CONFIGURAÃ‡Ã•ES PARA O CORPO DO DOCUMENTO                    %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\AtBeginDocument{
    \normalsize
    %% Configura para utilizar o cabeÃ§alho e rodapÃ© definidos anteriormente.    
    \pagestyle{fancy}
    %% Define o mÃ³dulo de citaÃ§Ã£o abreviado(Harvard)
    %% \citationmode{abbr}
    %% Define o estilo bibligrÃ¡fico agsm(Harvard)
    %%\bibliographystyle{agsm}
}

%%------------------------------------------------------------------------------
%% Desabilita a utilizaÃ§Ã£o de arroba
%%------------------------------------------------------------------------------
\makeatother
